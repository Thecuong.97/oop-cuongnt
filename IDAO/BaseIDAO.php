<?php 

    interface BaseIDAO
    {
        public function insert($row);
        public function update($row);
        public function delete($row);
    }