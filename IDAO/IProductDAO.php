<?php 

    include_once "BaseIDAO.php";

    interface IProductDAO extends BaseIDAO
    {
        public function fillAll();
        public function finById($id);
    }