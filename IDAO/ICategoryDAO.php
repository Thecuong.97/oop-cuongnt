<?php 

    include_once "BaseIDAO.php";

    interface ICategoryDAO extends baseIDAO
    {
        public function finAll();
        public function finById($id);
    }