<?php 

    include_once "BaseIDAO.php";

    interface IAccessoryDAO extends BaseIDAO
    {
        public function finAll();
        public function finById($id);
    }