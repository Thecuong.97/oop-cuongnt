<?php

    include_once "../Contants/ContantDB.php";
    include_once "../IDAO/IDatabase.php";
    class Database implements IDatabase
    {   
        private static $instants;
        private $productTable;
        private $categoryTable;
        private $accessoryTable;
        
        //Chan viec tao mot Object su dung New
        private function __construct()
        {
            $this->productTable = array();
            $this->categoryTable = array();
            $this->accessoryTable = array();
        
        }

        public static function getInstants()
        {
            if (empty(self::$instants)) {
                self::$instants = new Database();
            }
            return self::$instants;
        }

        // Kiểm tra xem $row truyền vào có phải object hay không
        private function checkRow($row){
            if (!is_object($row) || empty($row->getType())) {
                return false;
            }
            $data = $this->searchTable($row->getType());
            if (!is_array($data)) {
                return false;
            }
            return true;
        }
        
        // Insert 
        public function insertTable($row)
        {   
            if (!$this->checkRow($row)) {
                return -1;
            }
            $data = $this->searchTable($row->gettype());
            if (is_array($data)) {
                return array_push($data,$row);
            }
            return -1;
        }

        public function selectByIdTable($name, $id)
        {
            $data = [];
            $data = $this->searchTable($name);
            if (!is_array($data)) {
                return $data;
            }
            $result = null;
            foreach ($data as $key => $value) {
                if($value->getId() == $id)
                {
                    $result = $value;
                    break;
                }
            }
            return $result; 
        }

        public function selectTable($name, $where=null)
        {
            $data = [];
            $data = $this->searchTable($name);
            if (!is_array($data)) {
                return $data;
            }
            $result = [];
            if ($where == null) {
                $result = $data;
            } else {
                
            }
            return $result;
        }

        public function updateTable($row)
        {
            if (!$this->checkRow($row)) {
                return -1;
            }
            $data = [];
            $data = $this->searchtable($row->getType());
            if (!is_array($data)) {
                return -1;
            }
           $index = -1;
           foreach($data as $key => $value){
               if ($value->getId() == $row->getId()) {
                   $index = $key;
                   break;
               }
               return $index;   
           }
        }

        public function deleteTable($row)
        {
            if (!$this->checkRow($row)) {
                return -1;
            }
            $data = [];
            $data = $this->searchTable($row->getType());
            $index = -1;
            if (!is_array($data)) {
                return -1;
            }
            foreach ($data as $key => $value) {
                if ($value->getId() == $row->getId()) {
                    $index = $key;
                    break;
                }
            }
            if($index = -1){
                return -1;
            }
            return array_push($data, $index, 1);
        }

        public function truncateTable($name)
        {
            $data = [];
            $data = $this->searchTable($name);
            if (is_array($data)) {
                $data = [];
            }
        }

        private function searchTable($row)
        {
            switch (strtolower($row)) {
                case PRODUCT:
                    return $this->productTable;
                    break;
                
                case CATEGORY:
                    return $this->categoryTable;
                    break;
                
                case ACCESSORY:
                    return $this->accessoryTable;
                    break;
                
                default:
                    $result = -1;
                    return $result;
                    break;
            }
        }

        public function updateTableById($name, $id)
        {
            $data = [];
            $data = $this->searchTable($name);
            if (!is_array($data)) {
                return $data;
            }
            $result = null;
            foreach ($data as $key => $value) {
                if($value->getId() == $id){
                    $result = $value;
                    break;
                }
            }
            return $result; 
        }
    }