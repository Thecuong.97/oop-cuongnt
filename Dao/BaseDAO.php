<?php 

    require "Database.php";
    include_once "../Entity/Accessory.php";
    include_once "../Entity/Category.php";
    include_once "../Entity/Product.php";

    abstract class BaseDAO
    {
        public function insert($row)
        {
            $data = Database::getInstants();
            return $data->insertTable($row);
        }

        public function update($row)
        {
            $data = Database::getInstants();
            return $data->updatTable($row);
        }

        public function delete($row)
        {
            $data = Database::getInstants();
            return $data->deleteTable($row);
        }
    }