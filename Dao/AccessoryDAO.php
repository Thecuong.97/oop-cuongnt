<?php 

    include_once "../IDAO/IAccessoryDAO.php";
    include_once "BaseDAO.php";

    // Khai báo class AccessoryDAO kế thừa BaseDao và IAccessoryDAO
    class AccessoryDAO extends BaseDAO implements IAccessoryDAO
    {   
        private static $instants;

        // Chặn việc tạo một Object bằng new
        private function __construct()
        {

        }
        
        public static function getInstants()
        {
            if(empty(self::$instants))
            {
                self::$instants = new AccessoryDAO();
            }
            return self::$instants;
        }

        // Lấy tất cả dữ liệu Accessory
        public function finAll()
        {
            $data = Database::getInstants();
            return $data->selectTable(ACCESSORY);
        }

        // Lấy dữ liệu Accessory theo id
        public function finById($id)
        {
            $data = Database::getInstants();
            return $data->selectByIdTable(ACCESSORY,$id);
        }
    }