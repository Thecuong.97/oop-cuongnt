<?php 

    include_once "BaseDAO.php";

    class ProductDAO extends BaseDAO
    {
        private static $instans;

        //Chan viec tao Object su dung new
        private function __construct()
        {

        }

        public static function getInstans() 
        {
            if (empty(self::$instans)) {
                self::$instans = new ProductDAO();
            }
            return self::$instans;
        }

        public function finAll()
        {   
            $data = Database::getInstans();
            return $data->selectTable('product');
        }

        public function finById($id)
        {
            $data = Database::getInstans();
            return $data->selectByIdTable(PRODUCT,$id);
        }
    }