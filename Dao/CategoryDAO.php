<?php 

    include_once "../IDAO/ICategoryDAO.php";
    include_once "BaseDAO.php";

    class CategoryDAO extends BaseDAO implements ICategoryDAO   
    {
        private static $instans;

        //Chan viec tao mot Object su dung New
        public function __construct()
        {

        }

        public static function getInstans()
        {
            if(empty(self::$instans)){
                self::$instans = new CategoryDAO();
            }
            return self::$instans;
        }

        public function findAll()
        {
            $data = Database::getInstans();
             return $data->selectTable('category');
        }

        public function finById($id)
        {
            $data = Database::getInstans();
            return $data->selectByIdTable(CATEGORY,$id);
        }
    }
