<?php 

    include "../DAO/ProductDAO.php";

    class ProductDAODemo
    {   
        //Chan viec tao mot Object su dung New
        public function __construct()
        {

        }

        public function insertTest()
        {
            $data = ProductDAO::getInstants();
            $product = new Product();
            $product->setId(1);
            $product->setName("San pham so 1");
            $product->setCategoryId(1);
            $product->setType(PRODUCT);
            return $data->insert($product);
        }

        public function updateTest()
        {
            $data = ProductDAO::getInstants();
            $product = new Product();
            $product->setId(1);
            $product->setName("San pham so 1 da update");
            $product->setCategoryId(1);
            $product->setType(PRODUCT);
            return $data->update($product);
        }

        public function finAllTest()
        {
            $data = ProductDAO::getInstans();
            $result = $data->finAll();
            return $result;
        }
    }

    $pro = new ProductDAODemo();
    $pro->insertTest();
    echo "Du lieu <br>";
    var_dump($pro->finAllTest());

    $pro->updateTest();
    echo "Du lieu <br>";
    var_dump($pro->finAllTest());