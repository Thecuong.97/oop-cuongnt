<?php

    require '../Entity/Product.php';

    class ProductDemo extends Product 
    {   
        private static $instance;

        //Chan viec tao mot Object su dung New
        public function __construct($id, $name, $categoryId)
        {
            $this->id = $id;
            $this->name = $name;
            $this->categoryId = $categoryId;
        }

        public function getInstance()
        {
            if(empty(self::$instance)){
                self::$instance = new Product();
            }
            return self::$instance;
        }
        
        public function createProductTest()
        {   
            $product = new Product();
            $product->setId("1");
            $product->setName("San pham ");
            $product->setcategoryId("1");
            $product->settype(PRODUCT);
            
        }

        public function printProduct($product)
        {
            if(isset($product))
            {
                echo $product->getId();
                echo $product->getName();
                echo $product->getCategoryId();
            }
        }
    } 