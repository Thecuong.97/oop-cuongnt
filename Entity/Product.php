<?php

    include_once "BaseRow.php";
    include_once "../IEnity/IProduct.php";
    class Product extends BaseRow implements Iproduct
    {
        private $categoryId;

        public function __construct()
        {

        }

        public function setCategoryId($categoryId)
        {
            $this->categoryId = $categoryId;
        }

        public function getCategoryId()
        {
            return $this->categoryId;
        }
    }  
